﻿using Microsoft.EntityFrameworkCore;

namespace baikiemtra.Models
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
        {
        }
        public DbSet<Accounts> Accounts { get; set; }
        public DbSet<customer> Customers { get; set; }
        public DbSet<Employees> employees { get; set; }
        public DbSet<logs> logs { get; set; }
        public DbSet<reports> report { get; set; }
        public DbSet<Transactions> transactions { get; set; }

    }
}


