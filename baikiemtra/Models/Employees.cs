﻿namespace baikiemtra.Models
{
    public class Employees
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string ContactandAdress { get; set; }
        public string UsernameandPassword { get; set; }
        public string Addtexthere { get; set; }
        public List<Transactions> Transactions { get; set; }
    }
}
