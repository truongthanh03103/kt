﻿namespace baikiemtra.Models
{
    public class reports
    {
        public string Id { get; set; }
        public Accounts accountID { get; set; }
        public logs logsID { get; set; }
        public Transactions transactionsID { get; set; }
        public string reportsName { get; set; }
        public DateTime reportDate { get; set; }

    }
}
