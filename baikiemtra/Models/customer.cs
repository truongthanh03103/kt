﻿namespace baikiemtra.Models
{
    public class customer
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string ContactandAdress { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string Addtexthere { get; set; }
        public List<Accounts> Accounts { get; set; }

    }
}
