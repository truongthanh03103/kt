﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace baikiemtra.Migrations
{
    /// <inheritdoc />
    public partial class ttt : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "employees",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    FirstName = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    LastName = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    ContactandAdress = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    UsernameandPassword = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Addtexthere = table.Column<string>(type: "nvarchar(max)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_employees", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "logs",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Transactional = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    logindate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    loginTime = table.Column<TimeOnly>(type: "time", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_logs", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "transactions",
                columns: table => new
                {
                    Id = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    name = table.Column<string>(type: "nvarchar(max)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_transactions", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Customers",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    FirstName = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    LastName = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    ContactandAdress = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Username = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Password = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Addtexthere = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    TransactionsId = table.Column<string>(type: "nvarchar(450)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Customers", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Customers_transactions_TransactionsId",
                        column: x => x.TransactionsId,
                        principalTable: "transactions",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "EmployeesTransactions",
                columns: table => new
                {
                    TransactionsId = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    employeesIDId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EmployeesTransactions", x => new { x.TransactionsId, x.employeesIDId });
                    table.ForeignKey(
                        name: "FK_EmployeesTransactions_employees_employeesIDId",
                        column: x => x.employeesIDId,
                        principalTable: "employees",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_EmployeesTransactions_transactions_TransactionsId",
                        column: x => x.TransactionsId,
                        principalTable: "transactions",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Accounts",
                columns: table => new
                {
                    Id = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    customerIDId = table.Column<int>(type: "int", nullable: false),
                    accountName = table.Column<string>(type: "nvarchar(max)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Accounts", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Accounts_Customers_customerIDId",
                        column: x => x.customerIDId,
                        principalTable: "Customers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "report",
                columns: table => new
                {
                    Id = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    accountIDId = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    logsIDId = table.Column<int>(type: "int", nullable: false),
                    transactionsIDId = table.Column<string>(type: "nvarchar(450)", nullable: true),
                    reportsName = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    reportDate = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_report", x => x.Id);
                    table.ForeignKey(
                        name: "FK_report_Accounts_accountIDId",
                        column: x => x.accountIDId,
                        principalTable: "Accounts",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_report_logs_logsIDId",
                        column: x => x.logsIDId,
                        principalTable: "logs",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_report_transactions_transactionsIDId",
                        column: x => x.transactionsIDId,
                        principalTable: "transactions",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateIndex(
                name: "IX_Accounts_customerIDId",
                table: "Accounts",
                column: "customerIDId");

            migrationBuilder.CreateIndex(
                name: "IX_Customers_TransactionsId",
                table: "Customers",
                column: "TransactionsId");

            migrationBuilder.CreateIndex(
                name: "IX_EmployeesTransactions_employeesIDId",
                table: "EmployeesTransactions",
                column: "employeesIDId");

            migrationBuilder.CreateIndex(
                name: "IX_report_accountIDId",
                table: "report",
                column: "accountIDId");

            migrationBuilder.CreateIndex(
                name: "IX_report_logsIDId",
                table: "report",
                column: "logsIDId");

            migrationBuilder.CreateIndex(
                name: "IX_report_transactionsIDId",
                table: "report",
                column: "transactionsIDId");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "EmployeesTransactions");

            migrationBuilder.DropTable(
                name: "report");

            migrationBuilder.DropTable(
                name: "employees");

            migrationBuilder.DropTable(
                name: "Accounts");

            migrationBuilder.DropTable(
                name: "logs");

            migrationBuilder.DropTable(
                name: "Customers");

            migrationBuilder.DropTable(
                name: "transactions");
        }
    }
}
